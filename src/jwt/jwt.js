import jwt from "jsonwebtoken";

const privateKey = process.env.PRIVATE_KEY
export const createToken = (data) => {
    return jwt.sign({data}, privateKey);
}

export const checkToken = (token) => jwt.verify(token, privateKey, (error, decoded) => error
);

export const decodeToken = (token) => {
    return jwt.decode(token);
}
export const verifyToken = (req, res, next) => {
    let { token } = req.headers;
    let check = checkToken(token);
    if (check == null) {
        next()
    } else {
        res.status(401).send(check.name)
    }
}