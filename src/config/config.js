import dotenv from 'dotenv'
dotenv.config()

export default {
    database: process.env.DATABASE,
    host: process.env.HOST,
    username: process.env.USERNAME,
    port: process.env.PORT,
    dialect: process.env.DIALECT,
    password: process.env.PASSWORD,
}