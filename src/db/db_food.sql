-- -------------------------------------------------------------
-- TablePlus 3.7.1(332)
--
-- https://tableplus.com/
--
-- Database: db_food
-- Generation Time: 2023-12-01 10:09:06.2870
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


CREATE TABLE `food` (
  `food_id` int NOT NULL AUTO_INCREMENT,
  `food_name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type_id` int DEFAULT NULL,
  PRIMARY KEY (`food_id`),
  KEY `type_id` (`type_id`),
  CONSTRAINT `food_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `food_type` (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `food_type` (
  `type_id` int NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `like_res` (
  `like_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `res_id` int DEFAULT NULL,
  `date_like` datetime DEFAULT NULL,
  PRIMARY KEY (`like_id`),
  KEY `user_id` (`user_id`),
  KEY `res_id` (`res_id`),
  CONSTRAINT `like_res_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `like_res_ibfk_2` FOREIGN KEY (`res_id`) REFERENCES `restaurant` (`res_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `orders` (
  `order_id` int NOT NULL AUTO_INCREMENT,
  `amount` int DEFAULT NULL,
  `codes` varchar(255) DEFAULT NULL,
  `arr_sub_id` varchar(255) DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `food_id` int DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `user_id` (`user_id`),
  KEY `food_id` (`food_id`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `rate_res` (
  `rate_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `res_id` int DEFAULT NULL,
  `amount` int DEFAULT NULL,
  `date_rate` datetime DEFAULT NULL,
  PRIMARY KEY (`rate_id`),
  KEY `user_id` (`user_id`),
  KEY `res_id` (`res_id`),
  CONSTRAINT `rate_res_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `rate_res_ibfk_2` FOREIGN KEY (`res_id`) REFERENCES `restaurant` (`res_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `restaurant` (
  `res_id` int NOT NULL AUTO_INCREMENT,
  `res_name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`res_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `sub_food` (
  `sub_id` int NOT NULL AUTO_INCREMENT,
  `sub_name` varchar(255) DEFAULT NULL,
  `sub_price` float DEFAULT NULL,
  `food_id` int DEFAULT NULL,
  PRIMARY KEY (`sub_id`),
  KEY `food_id` (`food_id`),
  CONSTRAINT `sub_food_ibfk_1` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `users` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `passwords` varchar(255) DEFAULT NULL,
  `user_token` text,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `food` (`food_id`, `food_name`, `image`, `price`, `description`, `type_id`) VALUES
('1', 'Mì Quảng', 'mi-quang.jpg', '5.99', 'Mì Quảng là một món ăn ngon', '1'),
('2', 'Phở Bò', 'pho-bo.jpg', '6.99', 'Phở Bò ngon và bổ dưỡng', '1'),
('3', 'Bánh Mì Hamburger', 'banh-mi-hamburger.jpg', '4.99', 'Bánh Mì Hamburger thơm ngon', '2'),
('4', 'Cơm Gà', 'com-ga.jpg', '7.99', 'Cơm Gà ngon và tươi ngon', '3');

INSERT INTO `food_type` (`type_id`, `type_name`) VALUES
('1', 'Appetizer'),
('2', 'Main Course'),
('3', 'Dessert'),
('4', 'Beverage');

INSERT INTO `like_res` (`like_id`, `user_id`, `res_id`, `date_like`) VALUES
('1', '1', '1', '2023-11-08 10:00:00'),
('2', '2', '1', '2023-11-08 11:30:00'),
('3', '1', '2', '2023-11-09 12:45:00'),
('4', '3', '3', '2023-11-10 14:30:00');

INSERT INTO `orders` (`order_id`, `amount`, `codes`, `arr_sub_id`, `user_id`, `food_id`) VALUES
('1', '2', 'ABC123', '1,2,3', '1', '1'),
('2', '1', 'DEF456', '4,5', '2', '2'),
('3', '3', 'GHI789', '6,7,8', '1', '3'),
('4', '1', 'JKL012', '9', '3', '2');

INSERT INTO `rate_res` (`rate_id`, `user_id`, `res_id`, `amount`, `date_rate`) VALUES
('1', '1', '1', '4', '2023-11-05 08:00:00'),
('2', '2', '1', '5', '2023-11-06 09:30:00'),
('3', '1', '2', '3', '2023-11-06 14:15:00'),
('4', '3', '3', '5', '2023-11-07 16:45:00');

INSERT INTO `restaurant` (`res_id`, `res_name`, `image`, `description`) VALUES
('1', 'Restaurant A', 'restaurant_a.jpg', 'Mô tả nhà hàng A'),
('2', 'Restaurant B', 'restaurant_b.jpg', 'Mô tả nhà hàng B'),
('3', 'Restaurant C', 'restaurant_c.jpg', 'Mô tả nhà hàng C'),
('4', 'Restaurant D', 'restaurant_d.jpg', 'Mô tả nhà hàng D');

INSERT INTO `sub_food` (`sub_id`, `sub_name`, `sub_price`, `food_id`) VALUES
('1', 'Subway Club', '6.99', '1'),
('2', 'BMT', '7.49', '1'),
('3', 'Veggie Delite', '5.99', '1'),
('4', 'Chicken Teriyaki', '7.99', '2'),
('5', 'Steak and Cheese', '8.49', '2'),
('6', 'Meatball Marinara', '6.99', '2'),
('7', 'BLT', '6.49', '3'),
('8', 'Turkey Breast', '6.99', '3');

INSERT INTO `users` (`user_id`, `full_name`, `email`, `passwords`, `user_token`) VALUES
('1', 'John Doe', 'johndoe@email.com', 'hashed_password_1', NULL),
('2', 'Jane Smith', 'janesmith@email.com', 'hashed_password_2', NULL),
('3', 'Alice Johnson', 'alicejohnson@email.com', 'hashed_password_3', NULL),
('4', 'Bob Wilson', 'bobwilson@email.com', 'hashed_password_4', NULL);



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;