# GET ALL LIKE RESTAURANT
{{host}}/users/all-like-restaurant

# GET LIKE/UNLIKE
{{host}}/users/like-restaurant/:id-restaurant

# POST RATE RESTAURANT
{{host}}/users/rate-restaurant/:id-restaurant
    {
        amount: number
    }

# GET RATE RESTAURANT
{{host}}/users/get-rate-restaurant

# POST ORDER
{{host}}/users/order
    {
        arr_sub_id: "1,2,3,...",
        food_id: number
    }