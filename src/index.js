import express from 'express'
const app = express()
const port = 8000
import cors from 'cors'
import multer from "multer";
// import {DataTypes, Sequelize} from "sequelize";
// import config from "./config/config.js";
// import initModels from "./models/init-models.js";
// import {sequelize} from "./config/db_connect.js";
import rootRouter from "./routes/rootRouter.js";

const upload = multer();
app.use(cors())
app.use(express.json())
// for parsing application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));
// for parsing multipart/form-data
app.use(upload.array());
app.use(express.static('public'));
app.use(rootRouter)


app.get('/hello', async (req, res) => {
    return 'Hello World!'
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})