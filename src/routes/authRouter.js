import express from "express";
import {checkUserExits, login, signup} from "../controller/authController.js";

const rootRouter = express.Router()

rootRouter.post('/login', login)
rootRouter.post('/signup', checkUserExits, signup)

export default rootRouter