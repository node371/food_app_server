import express from "express";
import initModels from "../models/init-models.js";
import {sequelize} from "../config/db_connect.js";
import users from "../models/users.js";
import {getUser, getLikeRes, likeRes, rateRes, getRateRes, createOrder} from "../controller/userController.js";
import {verifyToken} from "../jwt/jwt.js";

const userRouter = express.Router()
userRouter.get('/get-user', getUser) // test
userRouter.get('/auth', getUser) // test
userRouter.get('/all-like-restaurant',verifyToken, getLikeRes)
userRouter.get('/like-restaurant/:id',verifyToken, likeRes)
userRouter.post('/rate-restaurant/:id',verifyToken, rateRes)
userRouter.get('/get-rate-restaurant',verifyToken, getRateRes)
userRouter.post('/order',verifyToken, createOrder)

export default userRouter