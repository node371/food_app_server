import express from "express";
import userRouter from "./userRouter.js";
import authRouter from "./authRouter.js";

const rootRouter = express.Router()

rootRouter.use('/users', userRouter)
rootRouter.use('/auth', authRouter)

export default rootRouter