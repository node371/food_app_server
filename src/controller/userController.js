import initModels from "../models/init-models.js";
import {sequelize} from "../config/db_connect.js";
import {responseData} from "../config/response.js";
import {decodeToken} from "../jwt/jwt.js";
import _ from "lodash";
import {makeid} from "../helper/helper.js";

const models = initModels(sequelize)

const findUserHelper = async (req, include = []) => {
    const token = req.headers.token
    const infoUser = decodeToken(token)
    return await models.users.findOne({
        where: {
            email: infoUser.data.email,
        }, include: include
    })
}
const getUser = async (req, res) => {
    try {
        const result = await models.users.findAll()
        return responseData(res, 'success', result, 200)
    } catch (e) {
        console.log(e)
        responseData(res, 'err', {}, 404)
    }
}
// GET LIKE RESTAURANT
const getLikeRes = async (req, res) => {
    try {
        // const token = req.headers.token
        // const infoUser = decodeToken(token)
        // const result = await models.users.findOne({
        //     where: {
        //         email: infoUser.data.email,
        //     }, include: ['like_res']
        // })
        const result = await findUserHelper(req, ['like_res'])
        result.passwords = undefined
        result.user_id = undefined
        result.user_token = undefined
        if (!result) {
            responseData(res, 'err', {result: '404 Forbidden'}, 404)
        }
        return responseData(res, 'success', result, 200)
    } catch (e) {
        console.log(e)
        responseData(res, 'err', {}, 400)
    }
}
const likeRes = async (req, res) => {
    try {
        const idRes = +req.params.id
        if (typeof idRes !== "number" || !idRes) {
            return responseData(res, 'err', {status: 'Id invalid'}, 400)
        }
        // const token = req.headers.token
        // const infoUser = decodeToken(token)
        // const allLikeRes = await models.users.findOne({
        //     where: {
        //         email: infoUser.data.email,
        //     }, include: ['like_res']
        // })
        const allLikeRes = await findUserHelper(req, ['like_res'])
        const dataLike = _.get(allLikeRes, 'like_res', []).find(o => o.res_id === idRes)
        if (dataLike) {
            await models.like_res.destroy({
                where: {
                    user_id: allLikeRes.user_id,
                    res_id: idRes
                }
            })
            return responseData(res, 'success', {unlike: true, res_id: idRes}, 200)
        } else {
            const like_res = {
                user_id: allLikeRes.user_id,
                res_id: idRes,
                date_like: new Date()
            }
            await models.like_res.create(like_res)
            return responseData(res, 'success', {like_res}, 200)
        }
    } catch (e) {
        console.log(e)
        responseData(res, 'err', {}, 404)
    }
}
const rateRes = async (req, res) => {
    try {
        const amount = req.body.amount
        if (amount < 1 || amount > 5) {
            return responseData(res, 'err', {status: 'Amount invalid, amount must be between 1 and 5'}, 400)
        }
        const idRes = +req.params.id
        if (typeof idRes !== "number" || !idRes) {
            return responseData(res, 'err', {status: 'Id invalid'}, 400)
        }
        // const token = req.headers.token
        // const infoUser = decodeToken(token)
        // const allRateRes = await models.users.findOne({
        //     where: {
        //         email: infoUser.data.email,
        //     }, include: ['rate_res']
        // })
        const allRateRes = await findUserHelper(req, ['rate_res'])
        const dataRate = _.get(allRateRes, 'rate_res', []).find(o => o.res_id === idRes)
        if (dataRate) {
            // console.log(allRateRes.user_id)
            // console.log(idRes)
            const rate = await models.rate_res.findOne({
                where: {
                    user_id: allRateRes.user_id,
                    res_id: idRes
                }
            })
            await rate.update({'amount': amount});
            await rate.save()
            // console.log('rate')
            return responseData(res, 'success', {rate}, 200)
        } else {
            // console.log('rate else')
            const rate_res = {
                user_id: allRateRes.user_id,
                res_id: idRes,
                amount,
                date_rate: new Date()
            }
            await models.rate_res.create(rate_res)
            return responseData(res, 'success', {rate_res}, 200)
        }
    } catch (e) {
        console.log(e)
        responseData(res, 'err', {}, 404)
    }
}
const getRateRes = async (req, res) => {
    try {
        // const token = req.headers.token
        // const infoUser = decodeToken(token)
        // const result = await models.users.findOne({
        //     where: {
        //         email: infoUser.data.email,
        //     }, include: ['rate_res']
        // })
        const result = await findUserHelper(req, ['rate_res'])
        result.passwords = undefined
        result.user_id = undefined
        result.user_token = undefined
        if (!result) {
            responseData(res, 'err', {}, 400)
        }
        return responseData(res, 'success', result, 200)
    } catch (e) {
        console.log(e)
        responseData(res, 'err', {}, 404)
    }
}

const createOrder = async (req, res) => {
    try {
        const dataOrder = req.body
        // console.log(dataOrder)
        dataOrder.codes = makeid(6).toUpperCase()
        // const token = req.headers.token
        // const infoUser = decodeToken(token)
        const infoUser = await findUserHelper(req, [])
        let price = 0
        dataOrder.arr_sub_id = dataOrder.arr_sub_id.split(',').map(o => o.trim())
        // check valid sub_id
        const validSubId = dataOrder.arr_sub_id.every((element) => !isNaN(element));
        if (!validSubId) return responseData(res, 'err', {status: 'sub_id valid'}, 400)
        // get total price
        for (const sub_id of dataOrder.arr_sub_id) {
            const data = await models.food.findOne({
                where: {
                    food_id: +sub_id,
                }
            })
            price = price + data.price
        }
        const main_food = await models.food.findOne({
            where: {
                food_id: dataOrder.food_id,
            }
        })
        price = price + main_food.price
        dataOrder.amount = price
        // console.log({user_id: infoUser.user_id})
        const result = await models.orders.create({
            ...dataOrder,
            arr_sub_id: dataOrder.arr_sub_id.join(),
            user_id: infoUser.user_id
        })
        if (!result) {
            responseData(res, 'err', {}, 400)
        }
        return responseData(res, 'success', result, 200)
    } catch (e) {
        console.log(e)
        responseData(res, 'err', {}, 404)
    }
}
export {
    getUser,
    likeRes,
    getLikeRes,
    rateRes,
    createOrder,
    getRateRes
}