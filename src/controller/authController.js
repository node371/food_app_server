import {responseData} from "../config/response.js";
import bcrypt from "bcrypt";
import {createToken} from "../jwt/jwt.js";
import initModels from "../models/init-models.js";
import {sequelize} from "../config/db_connect.js";
const models = initModels(sequelize)


export const checkUserExits = async (req, res, next) => {
    try {
        const checkUser = await models.users.findOne({where: {email: req.body.email}})
        if (checkUser) {
            return responseData(res, 'User already exists', {}, 404)
        }
        next()
    } catch (e) {
        return responseData(res, 'err', {}, 404)
    }
}
export const login = async (req, res) => {
    try {
        const {email, passwords} = req.body
        console.log({email, passwords})
        const user = await models.users.findOne({where: {email}})
        // console.log(user)
        if (!user) {
            return responseData(res, 'User not found', {}, 404)
        }
        if (!bcrypt.compareSync(passwords, user.passwords)) {
            return responseData(res, 'Password not match', {}, 404)
        }
        const token = createToken({email, full_name: user.full_name})
        await models.users.update({user_token: token}, {where: {email}})
        // console.log(token)
        const body = {email, full_name: user.full_name, token}
        return responseData(res, 'success', {data: body}, 200)
    } catch (e) {
        return responseData(res, 'err', {}, 404)
    }
}
export const signup = async (req, res) => {
    try {
        const {email, password, full_name} = req.body
        const passwordHash = bcrypt.hashSync(password, 10)
        const token = createToken({email, full_name})
        const body = {email, passwords: passwordHash, user_token: token, full_name}
        const user = await models.users.create(body)
        return responseData(res, 'success', {data: user}, 200)
    } catch (e) {
        console.log(e)
        responseData(res, 'err', {}, 404)
    }
}